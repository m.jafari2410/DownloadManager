/**
 * This class extends class ProcessingFrame and create a grid panel to update center panel of the main frame.
 *
 * @author Mohammad Jafari
 * @version 1
 * @since 31.5.2018
 */

import javax.swing.*;
import java.awt.*;
import java.util.Observable;
import java.util.Observer;

public class FoundPanel extends ProcessingFrame implements Observer {

    @Override
    public void update() {
        floatPanel = new JPanel(new FlowLayout());
        floatPanel.setBackground(Color.white);

        for (DownloadInfo downloadInfo : DownloadManager.foundDownloads) {
            if (downloadInfo.isFinished()) {
                CompletedItemFrame foundDownload = new CompletedItemFrame(downloadInfo);
                foundDownload.addObserver(this);
                floatPanel.add(foundDownload);
                continue;
            } else {
                DownloadingItemFrame foundDownload = new DownloadingItemFrame(downloadInfo);
                foundDownload.addObserver(this);
                floatPanel.add(foundDownload);
            }

        }
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof CompletedItemFrame) {
            floatPanel.remove((CompletedItemFrame) arg);
            this.revalidate();
            this.validate();
            this.repaint();
            return;
        }
        super.update(o, arg);
    }
}
