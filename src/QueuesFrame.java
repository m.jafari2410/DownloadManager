/**
 * This class is used to show downloading files in queue part of the program.
 * And also it extends class showList and implements the interface Observer to implement design pattern of observer
 * for updating list of downloads.
 *
 * @author Mohammad Jafari
 * @version 1.0
 * @since 5.9.2018
 */

import javax.swing.*;
import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Observable;
import java.util.Observer;

public class QueuesFrame extends ShowList implements Observer {
    private ProgressDownload progressDownload;
    private Queue queue;

    /**
     * This is constructor and adds this panel to the floatPanel and at the end this would be the centerPanel of the class DMFrame
     */
    public QueuesFrame(Queue queue) {
        super();
        this.queue = queue;
        update();
        infoObservable = new InfoObservable();
        this.add(floatPanel);
    }


    @Override
    public void update() {
        floatPanel = new JPanel(new FlowLayout());
        floatPanel.setBackground(Color.white);

        if (!queue.isScheduling()) {
            for (DownloadInfo downloadInfo : queue.getDownloads()) {
                DownloadingItemFrame downloadingItemFrame = new DownloadingItemFrame(downloadInfo);
                downloadingItemFrame.addObserver(this);
                floatPanel.add(downloadingItemFrame);

                progressDownload = new ProgressDownload(downloadInfo, downloadingItemFrame, this);
                progressDownload.addPropertyChangeListener(new ProgressListener(downloadingItemFrame, downloadInfo));

                if (DownloadManager.currentNumOfDownloads < Integer.valueOf(DownloadManager.limitDownloadNumber))
                    progressDownload.execute();
            }
        } else {
            for (DownloadInfo downloadInfo : queue.getDownloads()){
                if (!queue.getQueueTimeChecker().getStartDownload(downloadInfo,this)){
                    InQueueItem inQueueItem = new InQueueItem(downloadInfo);
                    inQueueItem.addObserver(this);
                    floatPanel.add(inQueueItem);
                } else {
                    DownloadingItemFrame downloadingItemFrame = queue.getQueueTimeChecker().getDownloadingItemFrame();
                    downloadingItemFrame.addObserver(this);
                    floatPanel.add(downloadingItemFrame);
                }
            }
        }
    }

    /**
     * This method of interface of Observable is implemented here to update the list of downloads when
     * list of downloads of the queue is changed or when namePanel of class InQueueItem is right clicked.
     */
    @Override
    public void update(Observable o, Object arg) {
        if (arg.equals("update queue")) {
            update();
            this.validate();
            this.revalidate();
            this.repaint();
        }
        if (arg instanceof DownloadInfo) {
            infoObservable.notifyObservers(arg);
            return;
        }
        if (!(arg instanceof DownloadingItemFrame))
            return;
        DownloadingItemFrame downloadItem = (DownloadingItemFrame) arg;
        this.floatPanel.remove(downloadItem);
        this.revalidate();
        this.repaint();
    }

    public Queue getQueue() {
        return queue;
    }
}
