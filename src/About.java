/**
 * This class give some details about the program and extends JFrame class
 *
 * @author Mohammad Jafari
 * @version 1.0
 * @since 5.9.2018
 */

import javax.swing.*;
import java.awt.*;

public class About extends JFrame {
    public About(){
        super("About");
        this.setSize(350,350);

        JPanel aboutUs = new JPanel(new BorderLayout(5,5));
        aboutUs.setBackground(Color.magenta);
        JLabel nameLabel = new JLabel("Purple Download Manager");
        JLabel versionLabel = new JLabel("version 3.0, 6.6.2018");
        JLabel aboutMe = new JLabel( "Author is Mohammad Jafari and this is written for \n midterm Project of AP course");
        this.setContentPane(aboutUs);
        this.add(nameLabel,BorderLayout.NORTH);
        this.add(versionLabel,BorderLayout.WEST);
        this.add(aboutMe,BorderLayout.SOUTH);
        this.pack();
        this.setLocationRelativeTo(new ProcessingFrame());
        setVisible(true);
    }
}
