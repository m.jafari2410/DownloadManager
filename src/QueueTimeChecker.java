/**
 * This class is used to calculate whether time of the queue has been reached or not.
 *
 * @author Mohammad Jafari
 * @version 1
 * @since 6.4.2018
 */

import javax.swing.*;
import java.time.LocalDateTime;
import java.util.Iterator;
import java.util.Observable;
import java.util.Observer;

public class QueueTimeChecker implements Runnable, Observer {
    private Queue queue;
    private ProcessingFrame processingFrame;
    private Boolean pastSituation = true;
    private ProgressDownload progressDownload;
    private DownloadingItemFrame downloadingItemFrame;
    private TimeReached timeReached;

    public QueueTimeChecker(Queue queue) {
        this.queue = queue;
        timeReached = new TimeReached();

    }

    @Override
    public void run() {
        int queueStartHour = queue.getStartHour();
        int queueStartMinute = queue.getStartMin();
        int queueEndHour = queue.getEndHour();
        int queueEndMinute = queue.getEndMin();
        int hour;
        int minute;

        while (true) {
            hour = LocalDateTime.now().getHour();
            minute = LocalDateTime.now().getMinute();
            if (queueStartHour < hour && hour < queueEndHour && pastSituation) {
                setWait(false);
            } else if (hour == queueStartHour) {
                if (minute >= queueStartMinute && pastSituation) {
                    setWait(false);
                } else if (minute < queueStartMinute && !pastSituation) {
                    setWait(true);
                }
            } else if (hour == queueEndHour) {
                if (minute < queueEndMinute && pastSituation) {
                    setWait(false);
                } else if (minute >= queueEndMinute && !pastSituation) {
                    setWait(true);
                }
            } else if (!pastSituation && !(queueStartHour < hour && hour < queueEndHour)) {
                setWait(true);
            }
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    public void setWait(Boolean set) {
        for (DownloadInfo downloadInfo : queue.getDownloads())
            downloadInfo.setWaiting(set);
        pastSituation = set;
        if (!pastSituation)
            timeReached.notifyObservers(queue);
    }

    public Boolean getStartDownload(DownloadInfo downloadInfo, QueuesFrame queuesFrame) {
        if (!pastSituation) {
            downloadingItemFrame = new DownloadingItemFrame(downloadInfo);
            progressDownload = new ProgressDownload(downloadInfo, downloadingItemFrame, queuesFrame);
            progressDownload.addPropertyChangeListener(new ProgressListener(downloadingItemFrame, downloadInfo));
            if (DownloadManager.currentNumOfDownloads < Integer.valueOf(DownloadManager.limitDownloadNumber)) {
                progressDownload.execute();
            }
            return true;
        } else
            return false;
    }

    public DownloadingItemFrame getDownloadingItemFrame() {
        return downloadingItemFrame;
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg.equals("one added"))
            setWait(pastSituation);
    }

    public class TimeReached extends Observable {
        @Override
        public void notifyObservers(Object arg) {
            setChanged();
            super.notifyObservers(arg);
        }
    }

    public void addObserver(Observer observer) {
        timeReached.addObserver(observer);
    }
}
