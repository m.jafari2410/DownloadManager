/**
 * This is Main class and contains main method and also it has a method that is used to set look and feel of the program.
 *
 * @author Mohammad Jafari
 * @version 1.0
 * @since 5.9.2018
 */

import javax.swing.*;
import java.awt.*;
import java.time.LocalDateTime;

public class Main {
    public static DMFrame frame;

    /**
     * This is main method and sets the main frame
     * @param args
     */
    public static void main(String[] args)  {

//        DownloadManager.queues.add(new Queue("default", 1 + "", 2 + ""));
//        DownloadManager.queues.add(new Queue("no", 1 + "", 2 + ""));
//        DownloadManager.queues.add(new Queue("yes", 1 + "", 2 + ""));
//        DownloadManager.queues.add(new Queue("jjj", 1 + "", 2 + ""));
//
//        DownloadInfo downloadInfo = new DownloadInfo("first", "www.google.com", "c:/pic");
//        DownloadInfo downloadInfo2 = new DownloadInfo("two", "www.google.com", "c:/Users");
//        DownloadInfo downloadInfo3 = new DownloadInfo("three", "www.google.com", "c:/pic3");
//        DownloadInfo downloadInfo4 = new DownloadInfo("for", "www.google.com", "c:/pic2");
//        DownloadInfo downloadInfo5 = new DownloadInfo("five.txt", "www.google.com", "c:/pic1");
//        downloadInfo5.setFinished(true);
//
//        DownloadManager.queues.get(0).add(downloadInfo);
//        DownloadManager.queues.get(0).add(downloadInfo2);
//        DownloadManager.completedDownloads.add(downloadInfo5);
//        DownloadManager.autoDownloads.add(downloadInfo3);
//        DownloadManager.autoDownloads.add(downloadInfo4);
        //loading list of downloads
        ListsMaintain listsMaintain = new ListsMaintain();
        listsMaintain.load();

        //loading list of queues
        QueueListMaintain queueListMaintain = new QueueListMaintain();
        queueListMaintain.load();

        //loading saved setting
        SettingMaintain settingMaintain = new SettingMaintain();
        settingMaintain.load();

        //loading list of removed downloads
        RemovedListMaintain removedListMaintain = new RemovedListMaintain();
        removedListMaintain.load();

        //loading list of filtered sites
        FilterSitesMaintain filterSitesMaintain = new FilterSitesMaintain();
        filterSitesMaintain.load();


        frame = new DMFrame();
        ImageIcon imageIcon = new ImageIcon("Images/purple.png");
        frame.setIconImage(imageIcon.getImage());


        try {
            uiSet();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }

        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setResizable(false);
        frame.setVisible(true);
    }

    /**
     * This method is used to set the look and feel of the program
     * @throws ClassNotFoundException  is thrown when look and feel return class not found
     * @throws UnsupportedLookAndFeelException is thrown when look and feel is not supported
     * @throws InstantiationException   Thrown when an application tries to create an instance of a class using the newInstance method in class Class, but the specified class object cannot be instantiated
     * @throws IllegalAccessException   s thrown when an application tries to reflectively create an instance (other than an array), set or get a field, or invoke a method, but the currently executing method does not have access to the definition of the specified class, field, method or constructor.
     */
    public static void uiSet() throws ClassNotFoundException, UnsupportedLookAndFeelException, InstantiationException, IllegalAccessException {
        switch (Setting.getLookAndFeel()) {
            case "Metal":
                UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
                SwingUtilities.updateComponentTreeUI(frame);
                break;
            case "Nimbus":
                UIManager.setLookAndFeel("javax.swing.plaf.nimbus.NimbusLookAndFeel");
                SwingUtilities.updateComponentTreeUI(frame);
                break;
            case "CDE/Motif":
                UIManager.setLookAndFeel("com.sun.java.swing.plaf.motif.MotifLookAndFeel");
                SwingUtilities.updateComponentTreeUI(frame);
                break;
            case "Window":
                UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsLookAndFeel");
                SwingUtilities.updateComponentTreeUI(frame);
                break;
            case "Windows Classic":
                UIManager.setLookAndFeel("com.sun.java.swing.plaf.windows.WindowsClassicLookAndFeel");
                SwingUtilities.updateComponentTreeUI(frame);
                break;
        }
    }
}
