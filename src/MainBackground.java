/**
 * This class creates a panel for background picture.
 *
 * @author Mohammad Jafari
 * @version 1.0
 * @since 5.9.2018
 */

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class MainBackground extends JPanel {
    private BufferedImage image;

    public MainBackground() {
        try {
            //reading the input
            image = ImageIO.read(new File("Images/background2.jpg"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * printing the panel by calling the paintComponent.
     */
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawImage(image, 0, 0, null);
    }
}
