/**
 * This class creates a JFrame for creating new queue and implements it.
 *
 * @author Mohammad Jafari
 * @version 1.0
 * @since 5.9.2018
 */


import javafx.beans.InvalidationListener;
import javafx.beans.Observable;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Observer;

public class QueueCreatorFrame extends JFrame implements ActionListener {
    //start and end time of the queue
    private JLabel startTimeLabel;
    private JLabel endTimeLabel;
    private JSpinner startSpinnerHour;
    private JSpinner startSpinnerMin;
    private JSpinner endSpinnerHour;
    private JSpinner endSpinnerMin;
    private JTextField nameField;
    //enable setting time for queue
    private JCheckBox enableTik;
    //for notifying the observer that list of the queues has changed
    private QueueListObservable observable;

    /**
     * This is constructor and added a lot of panels such as namePanel to the contentPane.
     */
    public QueueCreatorFrame() {
        super("Queue");
        this.setSize(300, 300);
        //setting the contentPane
        JPanel contentPane = new JPanel(new BorderLayout(5, 5));
        contentPane.setBorder(new EmptyBorder(20, 10, 10, 40));
        this.setContentPane(contentPane);
        this.getContentPane().setBackground(Color.white);
        this.setResizable(false);
        //implementing name panel
        JLabel nameLabel = new JLabel("Name of queue:");
        nameLabel.setOpaque(true);
        nameLabel.setBackground(Color.white);
        nameField = new JTextField();
        //if user entered in nameField of queue
        nameField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                QueueCreatorFrame.this.actionPerformed(e);
            }
        });
        JPanel namePanel = new JPanel(new BorderLayout(5, 5));
        namePanel.add(nameLabel, BorderLayout.WEST);
        namePanel.add(nameField, BorderLayout.CENTER);
        namePanel.setBorder(new EmptyBorder(0, 0, 10, 0));
        namePanel.setBackground(Color.white);

        //enable tik for time setting of queue
        enableTik = new JCheckBox("Set time for queue");
        enableTik.setBackground(Color.white);
        namePanel.add(enableTik, BorderLayout.SOUTH);

        startTimeLabel = new JLabel("Start time (Hour:Minute)");
        startTimeLabel.setBackground(Color.white);
        endTimeLabel = new JLabel("End time (Hour:Minute)");
        endTimeLabel.setBackground(Color.white);

        //providing 2 lists for hour and minute models of the Spinners.
        Integer hours[] = new Integer[24];
        Integer minutes[] = new Integer[60];
        for (int i = 0; i <= 23; i++)
            hours[i] = i;
        for (int i = 0; i <= 59; i++)
            minutes[i] = i;
        //hour and minute models of the spinner
        SpinnerListModel hour = new SpinnerListModel(hours);
        SpinnerListModel hour1 = new SpinnerListModel(hours);
        SpinnerListModel minute = new SpinnerListModel(minutes);
        SpinnerListModel minute1 = new SpinnerListModel(minutes);

        startSpinnerHour = new JSpinner(hour);
        startSpinnerHour.setBackground(Color.white);
        startSpinnerMin = new JSpinner(minute);
        startSpinnerMin.setBackground(Color.white);
        endSpinnerHour = new JSpinner(hour1);
        endSpinnerHour.setBackground(Color.white);
        endSpinnerMin = new JSpinner(minute1);
        endSpinnerMin.setBackground(Color.white);

        //panel1 is for start time setting
        JPanel panel1 = new JPanel(new GridLayout(1, 3, 2, 2));
        panel1.add(startTimeLabel);
        panel1.add(startSpinnerHour);
        panel1.add(startSpinnerMin);
        panel1.setBackground(Color.white);
        panel1.setBorder(new EmptyBorder(0, 0, 10, 0));
        panel1.setPreferredSize(new Dimension(panel1.getPreferredSize().width, panel1.getPreferredSize().height + 5));

        //panel2 is for end time setting
        JPanel panel2 = new JPanel(new GridLayout(1, 3, 2, 2));
        panel2.add(endTimeLabel);
        panel2.add(endSpinnerHour);
        panel2.add(endSpinnerMin);
        panel2.setBorder(new EmptyBorder(0, 0, 10, 0));
        panel2.setBackground(Color.white);

        JPanel centerPanel = new JPanel(new BorderLayout(5,5));
        centerPanel.setBackground(Color.white);
        centerPanel.add(panel1,BorderLayout.NORTH);
        centerPanel.add(panel2,BorderLayout.SOUTH);

        //set time setting disable for first
        setEnable(false);

        //handle enable tik
        // if tik was been selected then set the time setting enable
        enableTik.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (enableTik.isSelected())
                    setEnable(true);
                else
                    setEnable(false);
            }
        });

        JPanel lastPanel = new JPanel(new GridLayout(1, 2));
        JButton closeButton = new JButton("Cancel");
        JButton okButton = new JButton("Ok");
        okButton.addActionListener(this);
        lastPanel.add(closeButton);
        closeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                QueueCreatorFrame.super.setVisible(false);
            }
        });
        lastPanel.add(okButton);
        lastPanel.setBackground(Color.white);

        JPanel northOverallPanel = new JPanel(new BorderLayout(5,5));
        northOverallPanel.setBackground(Color.white);
        northOverallPanel.add(namePanel,BorderLayout.NORTH);
        northOverallPanel.add(centerPanel,BorderLayout.SOUTH);

        observable = new QueueListObservable();

        this.add(northOverallPanel,BorderLayout.NORTH);
        this.add(lastPanel,BorderLayout.EAST);
        this.pack();
        this.setResizable(false);
        this.setLocationRelativeTo(new ProcessingFrame());
        this.setVisible(true);
    }

    /**
     * If user entered or selected ok button then this method is done. if the name field was empty
     * it won't accept. unless queue is created.
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (this.nameField.getText().isEmpty())
            return;
        if (enableTik.isSelected()) {
            new Queue(this.nameField.getText(), (int) startSpinnerHour.getValue(), (int) startSpinnerMin.getValue(), (int) endSpinnerHour.getValue(), (int) endSpinnerMin.getValue());
        } else {
            new Queue(this.nameField.getText());
        }
        observable.notifyObservers("update queues");
        this.setVisible(false);
    }


    /**
     * This class is used to create an object of class Observable in class QueueCreatorFrame in order to
     * notify observers( i.e class DMFrame) of changing the list of the queues.
     *
     * @author Mohammad Jafari
     * @version 1.0
     * @since 5.9.2018
     */
    private class  QueueListObservable extends java.util.Observable {
        @Override
        public void notifyObservers(Object arg) {
            setChanged();
            super.notifyObservers(arg);
        }
    }

    /**
     * This method is used enable or disable the time setting of the queue in according to
     * the enable tik.
     */
    public void setEnable(Boolean enable) {
        startTimeLabel.setEnabled(enable);
        endTimeLabel.setEnabled(enable);
        startSpinnerHour.setEnabled(enable);
        startSpinnerMin.setEnabled(enable);
        endSpinnerHour.setEnabled(enable);
        endSpinnerMin.setEnabled(enable);
    }

    /**
     * This method is for adding observer to observables of class QueueListObservable
     */
    public void addObserver(Observer obs) {
        observable.addObserver(obs);
    }
}
