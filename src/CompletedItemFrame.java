/**
 * This class is used to implement panel of each file that it's download has been completed.
 *
 * @author Mohammad Jafari
 * @version 2.0
 * @since 5.30.2018
 */

import jdk.nashorn.internal.runtime.regexp.joni.Option;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.plaf.metal.MetalBorders;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;

public class CompletedItemFrame extends JPanel{
    public StatusObservable observable;

    /**
     * This is constructor and implements the panel
     * @param downloadInfo
     */
    public CompletedItemFrame(DownloadInfo downloadInfo) {
        //setting the layout manager of the jPanel
        super(new BorderLayout(5, 5));
        Border border = BorderFactory.createLineBorder(Color.white.darker(), 2);
        this.setBorder(border);
        this.setBackground(Color.white);

        observable = new StatusObservable();
        //nameLabel for name of the download
        JLabel nameLabel = new JLabel(downloadInfo.getName());
        //adding mouse handler
        nameLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                if (e.getButton() == MouseEvent.BUTTON1) {
                    File file = new File(downloadInfo.getStorageAddress() + "/" + downloadInfo.getName());
                    if (file.exists()) {
                        try {
                            Desktop.getDesktop().open(file);
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }
                    } else
                        JOptionPane.showMessageDialog(new JFrame(), "this file Doesn't exist more", "File Not Found", JOptionPane.ERROR_MESSAGE);
                } else if (e.getButton() == MouseEvent.BUTTON3){
                    observable.notifyObservers(downloadInfo);
                }
            }

            @Override
            public void mouseEntered(MouseEvent e) {
                nameLabel.setBorder(BorderFactory.createLineBorder(Color.getHSBColor(203,64,125),3));
            }

            @Override
            public void mouseExited(MouseEvent e) {
                nameLabel.setBorder(null);
            }
        });
        //for starting time
        JLabel timeLabel = new JLabel(downloadInfo.getStartTime());
        JPanel detailPanel = new JPanel(new BorderLayout(5, 5));
        detailPanel.setBackground(Color.white);
        detailPanel.add(nameLabel, BorderLayout.WEST);
        detailPanel.add(timeLabel, BorderLayout.EAST);
        //adding buttons and icons
        JPanel southPanel = new JPanel(new BorderLayout(5, 5));
        southPanel.setBackground(Color.white);
        ImageIcon playImage = new ImageIcon("Images/play.png");
        ImageIcon folderImage = new ImageIcon("Images/folder.png");
        ImageIcon deleteImage = new ImageIcon("Images/delete.png");
        JButton resumeButton = new JButton(playImage);
        //handling resume button
        resumeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                File file = new File(downloadInfo.getStorageAddress() + "/" + downloadInfo.getName());
                if (file.exists()) {
                    try {
                        Desktop.getDesktop().open(file);
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                } else
                    JOptionPane.showMessageDialog(new JFrame(), "this file Doesn't exist more", "File Not Found", JOptionPane.ERROR_MESSAGE);
            }
        });
        JButton folderButton = new JButton(folderImage);
        //handling folder button
        folderButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                File file = new File(downloadInfo.getStorageAddress());
                if (!file.exists())
                    file.mkdir();
                try {
                    Desktop.getDesktop().open(file);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });
        //for deleting download file item
        JButton deleteButton = new JButton(deleteImage);
        deleteButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DownloadManager.completedDownloads.remove(downloadInfo);
                observable.notifyObservers(CompletedItemFrame.this);
                DownloadManager.removedDownloads.add(downloadInfo);
                new RemovedListMaintain().save();
            }
        });
        JPanel southWestPanel = new JPanel(new GridLayout(1, 3));
        southWestPanel.setBackground(Color.white);
        southWestPanel.add(resumeButton);
        southWestPanel.add(folderButton);
        southWestPanel.add(deleteButton);
        southPanel.add(southWestPanel, BorderLayout.WEST);
        String relativeDownloadSize = String.format("%dKb", downloadInfo.getSize());
        JLabel sizeLabel = new JLabel(relativeDownloadSize);
        southPanel.add(sizeLabel, BorderLayout.EAST);
        //image of the file that is get from the net
        JLabel ImgOfFile = new JLabel("Image");
        ImgOfFile.setBackground(Color.white);

        JPanel centerPanel = new JPanel(new BorderLayout(5, 5));
        centerPanel.setBackground(Color.white);
        centerPanel.add(detailPanel, BorderLayout.NORTH);
        centerPanel.add(southPanel, BorderLayout.SOUTH);

        this.add(ImgOfFile, BorderLayout.WEST);
        this.add(centerPanel, BorderLayout.CENTER);

        int newWidth = this.getPreferredSize().width;
        int newHeight = this.getPreferredSize().height;
        this.setPreferredSize(new Dimension(newWidth + 50, newHeight));
    }

    /**
     * this method add observer to the observable of StatusObservable
     * @param observer is add to observers
     */
    public void addObserver(Observer observer) {
        observable.addObserver(observer);
    }

    /**
     * This class is status observable serve as notifying observers(i.e class CompletedFrame) of changing download list
     * or when the InfoPanel should be showed.
     *
     * @author Mohammad Jafari
     * @version 1.0
     * @since 5.9.2018
     */
    public class StatusObservable extends java.util.Observable {
        @Override
        public void notifyObservers(Object arg) {
            setChanged();
            super.notifyObservers(arg);
        }
    }

}
