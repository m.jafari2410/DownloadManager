/**
 * This class creates a JFrame to get a string and then constructor of class FoundPanel to show the matches downloads in the main panel.
 *
 * @author Mohammad Jafari
 * @version 1.0
 * @since 31.5.2018
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.Observable;
import java.util.Observer;

public class SearchFrame extends JFrame {
    private EnterObservable enterObservable;

    public SearchFrame() {
        enterObservable = new EnterObservable();

        JPanel contentPane = new JPanel(new BorderLayout(5, 5));
        contentPane.setBackground(Color.WHITE);
        this.setContentPane(contentPane);
        contentPane.setBackground(Color.getHSBColor(0.8f, 0.30f, 1f));

        JLabel findLabel = new JLabel("Search for:");
        findLabel.setBackground(Color.white);

        JTextField findField = new JTextField();
        findField.setPreferredSize(new Dimension(500, 20));

        JButton findButton = new JButton("Find");
        findButton.setBackground(Color.getHSBColor(2000, 190, 50));

        contentPane.add(findLabel, BorderLayout.NORTH);
        contentPane.add(findField, BorderLayout.CENTER);
        contentPane.add(findButton, BorderLayout.EAST);

        ImageIcon imageIcon = new ImageIcon("Images/purple.png");
        this.setIconImage(imageIcon.getImage());

        this.pack();
        this.setLocationRelativeTo(new ProcessingFrame());
        this.setVisible(true);

        findButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DownloadManager.foundDownloads = new SearchDownload().findDownloads(findField.getText());
                SearchFrame.super.setVisible(false);
                enterObservable.notifyObservers(new FoundPanel());
            }
        });

        findField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    DownloadManager.foundDownloads = new SearchDownload().findDownloads(findField.getText());
                    SearchFrame.super.setVisible(false);
                    enterObservable.notifyObservers(new FoundPanel());
                }
            }
        });
    }

    public class EnterObservable extends Observable {
        @Override
        public void notifyObservers(Object arg) {
            setChanged();
            super.notifyObservers(arg);
        }
    }

    public void addObserver(Observer observer) {
        enterObservable.addObserver(observer);
    }
}
