/**
 * This is the main Frame that includes all frames of the program.
 *
 * @author Mohammad Jafari
 * @version 1.0
 * @since 5.9.2018
 */

import com.sun.jmx.mbeanserver.JmxMBeanServer;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.awt.image.ImageProducer;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;
import java.util.Set;

public class DMFrame extends JFrame implements Observer {
    private JMenuBar queueMenuBar;
    private JPanel queuePanel;
    private JMenu queueMenu;
    private static JPanel centerPanel;
    private TrayIcon trayIcon;
    private SystemTray systemTray;


    /**
     * This is constructor and implements the main frame.
     */
    public DMFrame() {
        super("Purple Download Manager");
        this.setSize(600, 600);
        //when close is clicked don't stop the program
        this.setDefaultCloseOperation(JFrame.ICONIFIED);
        //to create system icon in system tray when closing the frame
        this.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent windowEvent) {
                setExtendedState(JFrame.ICONIFIED);
                SystemTray systemTray = SystemTray.getSystemTray();
                BufferedImage image = null;
                try {
                    //loading image
                    image = ImageIO.read(new FileInputStream(new File("Images/purple1.png")));
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                } catch (IOException e) {
                    e.printStackTrace();
                }
                //creating popup menu in system tray
                PopupMenu popupMenu = new PopupMenu();
                MenuItem exitMenu = new MenuItem("Exit");
                MenuItem openItem = new MenuItem("Open");
                popupMenu.add(exitMenu);
                popupMenu.add(openItem);
                //handling exit from popup menu
                exitMenu.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        new ListsMaintain().save();
                        new QueueListMaintain().save();
                        new SettingMaintain().save();
                        System.exit(0);
                    }
                });
                //handling open from popup menu
                openItem.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        DMFrame.this.setVisible(true);
                    }
                });
                //adding image to the tray icon
                TrayIcon trayIcon = new TrayIcon(image);
                //handling double click on the tray icon
                trayIcon.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        DMFrame.this.setVisible(true);
                    }
                });
                trayIcon.setPopupMenu(popupMenu);
                try {
                    systemTray.add(trayIcon);

                } catch (AWTException e) {
                    e.printStackTrace();
                }
            }
        });

        //setting the contentPane of the frame
        JPanel contentPane = new JPanel(new BorderLayout(5, 5));
        contentPane.setBorder(new EmptyBorder(2, 2, 2, 2));
        this.setContentPane(contentPane);

        //adding menu bar by JMenuBar
        JMenuBar menuBar = new JMenuBar();
        JMenu download = new JMenu("Download");
        //this sets mnemonic for the item download others are like this
        download.setMnemonic(KeyEvent.VK_D);

        JMenu help = new JMenu("Help");
        help.setMnemonic(KeyEvent.VK_H);

        JMenuItem about = new JMenuItem("About");
        about.setMnemonic(KeyEvent.VK_A);

        JMenuItem newDownload = new JMenuItem("NewDownload");
        newDownload.setMnemonic(KeyEvent.VK_N);
        //this is used set a shortcut for new download in menu and others are like this
        newDownload.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, Event.CTRL_MASK));
        newDownload.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new NewDownloadFrame().addObserver(DMFrame.this::update);
            }
        });

        //like above adding other items for menu
        JMenuItem pause = new JMenuItem("Pause");
        pause.setMnemonic(KeyEvent.VK_P);

        JMenuItem resume = new JMenuItem("Resume");
        resume.setMnemonic(KeyEvent.VK_R);

        JMenuItem cancel = new JMenuItem("Cancel");
        cancel.setMnemonic(KeyEvent.VK_C);

        JMenuItem remove = new JMenuItem("Remove");
        remove.setMnemonic(KeyEvent.VK_V);

        JMenuItem settings = new JMenuItem("Settings");
        settings.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, Event.CTRL_MASK));


        JMenuItem exit = new JMenuItem("Exit");
        exit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new ListsMaintain().save();
                new QueueListMaintain().save();
                new SettingMaintain().save();
                System.exit(0);
            }
        });
        exit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, Event.CTRL_MASK));
        exit.setMnemonic(KeyEvent.VK_X);

        JMenuItem search = new JMenuItem("Search");
        search.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F, Event.CTRL_MASK));
        search.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new SearchFrame().addObserver(DMFrame.this::update);
            }
        });

        download.add(newDownload);
        download.add(pause);
        download.add(resume);
        download.add(search);
        download.add(cancel);
        download.add(remove);
        download.add(settings);
        download.add(exit);

        help.add(about);
        menuBar.add(download);
        menuBar.add(help);
        this.setJMenuBar(menuBar);
        //handling setting button. to run setting frame
        settings.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new Setting();
            }
        });
        //handling about button
        about.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new About();
            }
        });
        //loading images
        ImageIcon newDownloadImage = new ImageIcon("Images/plus.png");
        ImageIcon pauseImage = new ImageIcon("Images/pause.png");
        ImageIcon resumeImage = new ImageIcon("Images/play1.png");
        ImageIcon cancelImage = new ImageIcon("Images/cancel.png");
        ImageIcon removeImage = new ImageIcon("Images/delete1.png");
        ImageIcon settingImage = new ImageIcon("Images/setting.png");

        //creating main toolbar and adding buttons to that
        JToolBar mainToolBar = new JToolBar();
        JButton newDownloadButton = new JButton(newDownloadImage);
        newDownloadButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new NewDownloadFrame().addObserver(DMFrame.this::update);
            }
        });
        JButton pauseButton = new JButton(pauseImage);
        JButton resumeButton = new JButton(resumeImage);
        JButton cancelButton = new JButton(cancelImage);
        JButton removeButton = new JButton(removeImage);
        JButton settingsButton = new JButton(settingImage);
        mainToolBar.add(newDownloadButton);
        mainToolBar.add(pauseButton);
        mainToolBar.add(resumeButton);
        mainToolBar.add(cancelButton);
        mainToolBar.add(removeButton);
        mainToolBar.add(settingsButton);
        mainToolBar.setFloatable(false);
        mainToolBar.setRollover(true);
        settingsButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new Setting();
            }
        });

        JPanel northPanel = new JPanel(new BorderLayout(5, 5));
        northPanel.setBackground(Color.white);
        Border northBorder = BorderFactory.createLineBorder(Color.white.darker(), 2);
        northPanel.setBorder(northBorder);
        northPanel.add(mainToolBar, BorderLayout.NORTH);

        JPanel westPanel = new JPanel(new BorderLayout(5, 5));
        westPanel.setBackground(Color.white);
        Border westBorder = BorderFactory.createLineBorder(Color.white.darker(), 2);
        westPanel.setBorder(westBorder);
        JPanel westLists = new JPanel(new GridLayout(3, 1));
        westLists.setBackground(Color.white);
        JButton processButton = new JButton("Processing");
        processButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                updateCenter(new ProcessingFrame().addObserver(DMFrame.this::update));
            }
        });
        processButton.setBackground(Color.white);
        //for selecting to show completed downloads
        JButton completedButton = new JButton("Completed");
        completedButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                updateCenter(new CompletedFrame().addObserver(DMFrame.this::update));
            }
        });
        completedButton.setBackground(Color.white);
        //for showing queues
        queueMenuBar = new JMenuBar();
        queueMenu = new JMenu("Queues");
        updateQueuesList();
        queueMenuBar.add(queueMenu);
        ImageIcon queueImage = new ImageIcon("Images/calendar.png");
        JMenuItem newQueue = new JMenuItem(queueImage);
        newQueue.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                new QueueCreatorFrame().addObserver(DMFrame.this::update);
            }
        });
        queueMenuBar.add(newQueue);
        westLists.add(processButton);
        westLists.add(completedButton);
        westLists.add(queueMenuBar);

        westPanel.add(new MainBackground(), BorderLayout.CENTER);
        westPanel.add(westLists, BorderLayout.SOUTH);
        centerPanel = new ProcessingFrame().addObserver(this);
        this.add(northPanel, BorderLayout.NORTH);
        this.add(centerPanel, BorderLayout.CENTER);
        this.add(westPanel, BorderLayout.WEST);
        this.setPreferredSize(new Dimension(800, 600));

    }

    /**
     * this method update the center panel that is used to show processing downloads or
     * completed downloads
     *
     * @param panel is the panel whose has changed or has been clicked
     */
    public void updateCenter(ShowList panel) {
        this.getContentPane().remove(centerPanel);
        centerPanel = panel;
        this.add(centerPanel, BorderLayout.CENTER);
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                DMFrame.this.validate();
                DMFrame.this.revalidate();
                DMFrame.this.repaint();
            }
        });
    }

    /**
     * This method update list of queues when a queue is created.
     */
    public void updateQueuesList() {
            for (Queue queue : DownloadManager.queues) {
                JMenuItem item = new JMenuItem(queue.toString());
                queueMenu.add(item);
                queue.addObserver(this);
                item.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        updateCenter(new QueuesFrame(queue).addObserver(DMFrame.this::update));
                    }
                });
                item.setToolTipText("Starts at: " + queue.getStartHour() + ":" + queue.getStartMin() + " Stops at: " + queue.getEndHour() + ":" + queue.getEndMin());
            }
    }

    /**
     * This method is called when downloads or list of queues are changed.
     *
     * @param arg is used to find out which observable has been changed.
     */
    @Override
    public void update(Observable o, Object arg) {
        if (arg.equals("update queues")) {
            queueMenu.removeAll();
            updateQueuesList();
        } else if (arg.equals("update downloads")) {
            updateCenter(new ProcessingFrame().addObserver(this));
        } else if (arg instanceof DownloadInfo) {
            DownloadInfo downloadInfo = (DownloadInfo) arg;
            this.add(new InfoPanel(downloadInfo), BorderLayout.EAST);
            this.pack();
        } else if (arg instanceof FoundPanel) {
            updateCenter((FoundPanel) arg);
        } else if (arg instanceof Queue){
            Queue queue = (Queue) arg;
            updateCenter(new QueuesFrame(queue).addObserver(this));
        }
    }


}
