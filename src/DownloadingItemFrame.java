/**
 * This class is used to implement the panel of each file that is downloading.
 *
 * @author Mohammad Jafari
 * @version 2.0
 * @since 5.30.2018
 */

import javafx.beans.InvalidationListener;
import javafx.beans.Observable;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.text.Format;
import java.util.Observer;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class DownloadingItemFrame extends JPanel {
    //to notify observers(i.e ProcessingFrame) of the events to act
    public StatusObservable observable;
    private JProgressBar progressBar;
    private JLabel sizeLabel;
    private static ExecutorService pool = Executors.newCachedThreadPool();

    /**
     * This is constructor and implements the panel of the downloading files
     * @param downloadInfo is the downloading file
     */
    public DownloadingItemFrame(DownloadInfo downloadInfo) {
        super(new BorderLayout(5, 5));
        Border border = BorderFactory.createLineBorder(Color.white.darker(), 2);
        this.setBorder(border);
        this.setBackground(Color.white);
        JPanel centerPanel = new JPanel(new BorderLayout(3, 2));
        centerPanel.setBackground(Color.white);
        JLabel nameLabel = new JLabel(downloadInfo.getName());
        //adding mouse listener for name of the file to open the file or show info.
        nameLabel.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseReleased(MouseEvent e) {
                //left click
                if (e.getButton() == MouseEvent.BUTTON1){
                    File file = new File(downloadInfo.getStorageAddress());
                    if (!file.exists())
                        //if this directory doesn't exist more, make it
                        file.mkdir();
                    try {
                        //open the directory by using class desktop
                        Desktop.getDesktop().open(file);
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                //right click
                } else if (e.getButton() == MouseEvent.BUTTON3){
                    //notify observer (ProcessingPanel) to show the info
                    observable.notifyObservers(downloadInfo);
                }
            }
            //if mouse entered to the area of the name label, make a border for the label
            @Override
            public void mouseEntered(MouseEvent e) {
                nameLabel.setBorder(BorderFactory.createLineBorder(Color.getHSBColor(203,64,125),3));
            }
            //if mouse went out of the area, clean the border
            @Override
            public void mouseExited(MouseEvent e) {
                nameLabel.setBorder(null);
            }
        });
        nameLabel.setBackground(Color.white);
        progressBar = new JProgressBar(0, 100);
        progressBar.setStringPainted(true);
        progressBar.setForeground(Color.BLUE);
        UIManager.put("ProgressBar.background", Color.WHITE);
        UIManager.put("ProgressBar.foreground", Color.magenta);
        UIManager.put("ProgressBar.selectionBackground", Color.BLUE);
        UIManager.put("ProgressBar.selectionForeground", Color.WHITE);
        progressBar.setValue((int) downloadInfo.getPercent());
        progressBar.setStringPainted(true);
        //here update progressBar and must be done in phase 2
        JPanel southPanel = new JPanel(new BorderLayout(5, 5));
        southPanel.setBackground(Color.white);
        ImageIcon resumeImage = new ImageIcon("Images/play.png");
        ImageIcon stopImage = new ImageIcon("Images/stop.png");
        JToggleButton resumeButton = new JToggleButton(resumeImage);
        resumeButton.setIcon(resumeImage);
        resumeButton.setSelectedIcon(stopImage);

        resumeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (resumeButton.isSelected())
                    downloadInfo.setPaused(true);
                else if (!resumeButton.isSelected()) {
                    downloadInfo.setPaused(false);
                    Download newDownload = new Download(downloadInfo);
                    newDownload.connect();
                    pool.execute(newDownload);
                    observable.notifyObservers("update");
                }
            }
        });

        ImageIcon folderImage = new ImageIcon("Images/folder.png");
        ImageIcon deleteImage = new ImageIcon("Images/delete.png");
        //handling folder button and the delete button
        JButton folderButton = new JButton(folderImage);
        folderButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                File file = new File(downloadInfo.getStorageAddress());
                if (!file.exists())
                    file.mkdir();
                try {
                    Desktop.getDesktop().open(file);
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        });

        observable = new StatusObservable();
        //handling delete button
        JButton deleteButton = new JButton(deleteImage);
        deleteButton.addActionListener(new ActionListener() {
            @Override
            //notify the observer that the list of the downloads has been changed please repaint
            public void actionPerformed(ActionEvent e) {
                downloadInfo.setCancelled(true);
                if (DownloadManager.autoDownloads.contains(downloadInfo)) {
                    DownloadManager.autoDownloads.remove(downloadInfo);
                    observable.notifyObservers(DownloadingItemFrame.this);
                } else {
                    for (Queue queue : DownloadManager.queues) {
                        if (queue.getDownloads().contains(downloadInfo)) {
                            queue.getDownloads().remove(downloadInfo);
                            observable.notifyObservers(DownloadingItemFrame.this);
                            break;
                        }
                    }
                }
                DownloadManager.removedDownloads.add(downloadInfo);
                new RemovedListMaintain().save();
            }
        });
        JPanel southWestPanel = new JPanel(new GridLayout(1, 3));
        southWestPanel.setBackground(Color.white);
        southWestPanel.add(resumeButton);
        southWestPanel.add(folderButton);
        southWestPanel.add(deleteButton);

        southPanel.add(southWestPanel, BorderLayout.WEST);
        //downloading size and overall size of the file
        String relativeDownloadSize = String.format("%dKb / %dKb", downloadInfo.getDownloadedSize() / 1000, downloadInfo.getSize());
        sizeLabel = new JLabel(relativeDownloadSize);
        southPanel.add(sizeLabel, BorderLayout.EAST);

        centerPanel.add(nameLabel, BorderLayout.NORTH);
        centerPanel.add(progressBar, BorderLayout.CENTER);
        centerPanel.add(southPanel, BorderLayout.SOUTH);

        JLabel ImgOfFile = new JLabel("Image");
        ImgOfFile.setBackground(Color.white);
        //this label is used to show the percentage of the downloaded file
        JLabel percentage = new JLabel("             ");
        percentage.setBackground(Color.white);

        this.add(ImgOfFile, BorderLayout.WEST);
        this.add(centerPanel, BorderLayout.CENTER);
        this.add(percentage, BorderLayout.EAST);

        int maxHeight = this.getPreferredSize().height;
        int maxWidth = this.getPreferredSize().width;
        this.setMaximumSize(new Dimension(maxWidth, maxHeight));
    }

    /**
     * This method adds observer to the observable
     */
    public  void addObserver(Observer observer){
        observable.addObserver(observer);
    }

    /**
     * This class is status observable serve as notifying observers(i.e class ProcessingFrame) of changing download list
     * or when the InfoPanel should be showed.
     *
     * @author Mohammad Jafari
     * @version 1.0
     * @since 5.9.2018
     */
    public  class StatusObservable extends java.util.Observable {
        @Override
        public void notifyObservers(Object arg) {
            setChanged();
            super.notifyObservers(arg);
        }
    }

    public void setProgressBar(int i) {
        this.progressBar.setValue(i);
    }

    public void setRelativeDownloadSize(String relativeDownloadSize) {
        sizeLabel.setText(relativeDownloadSize);
    }
}
