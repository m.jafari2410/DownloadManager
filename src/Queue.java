/**
 * This class models a queue for download.
 *
 * @author Mohammad Jafari
 * @version 1.0
 * @since 5.9.2018
 */

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Observable;
import java.util.Observer;

public class Queue implements Serializable, Observer {
    private ArrayList<DownloadInfo> downloads;
    private int startHour;
    private int startMin;
    private int endHour;
    private int endMin;
    private String name;
    private Boolean isScheduling;
    private QueueTimeChecker queueTimeChecker;
    private QueueObservable queueObservable;
    private Thread timeThread;

    //no parameter constructor
    public Queue() {
        downloads = new ArrayList<DownloadInfo>();
    }

    /**
     * This is constructor and it makes queues and adds them to the lists of class DownloadManager
     * This constructor is for queues that has start and end times of downloads
     *
     * @param name      name of the queue
     * @param startHour start hour time of download
     * @param startMin  start minute time of download
     * @param endHour   end hour time of download
     * @param endMin    end minute time of download
     */
    public Queue(String name, int startHour, int startMin, int endHour, int endMin) {
        this.name = name;
        this.startHour = startHour;
        this.startMin = startMin;
        this.endHour = endHour;
        this.endMin = endMin;
        downloads = new ArrayList<DownloadInfo>();
        DownloadManager.queuesNames.add(name);
        DownloadManager.queues.add(this);
        isScheduling = true;
        queueObservable = new QueueObservable();

        queueTimeChecker = new QueueTimeChecker(this);
        queueTimeChecker.addObserver(this);
        queueObservable.addObserver(queueTimeChecker);
        timeThread = new Thread(queueTimeChecker);
        timeThread.start();
    }

    //overloading constructor for queues without start and end times of download
    public Queue(String name) {
        this.name = name;
        downloads = new ArrayList<DownloadInfo>();
        DownloadManager.queuesNames.add(name);
        DownloadManager.queues.add(this);
        isScheduling = false;
        queueObservable = new QueueObservable();

    }

    public Boolean isScheduling() {
        return isScheduling;
    }

    /**
     * adding a new download to the list of the queue.
     */
    public void add(DownloadInfo downloadInfo) {
        downloads.add(downloadInfo);
        queueObservable.notifyObservers("one added");
    }

    /**
     * This is a getter method and gets the list of downloads of this queue.
     *
     * @return list of downloads of this queue
     */
    public ArrayList<DownloadInfo> getDownloads() {
        return downloads;
    }

    /**
     * Gets start hour time of the queue
     *
     * @return
     */
    public int getStartHour() {
        return startHour;
    }

    /**
     * Gets start minute time of the queue
     *
     * @return
     */
    public int getStartMin() {
        return startMin;
    }

    /**
     * Gets end hour time of the queue
     *
     * @return
     */
    public int getEndHour() {
        return endHour;
    }

    /**
     * Gets end minute time of the queue
     *
     * @return
     */
    public int getEndMin() {
        return endMin;
    }

    /**
     * This method is overridden here and returns name of the queue
     *
     * @return name of the queue
     */
    @Override
    public String toString() {
        return name;
    }

    /**
     * This is a getter method and returns queueTimeChecker.
     *
     * @return
     */
    public QueueTimeChecker getQueueTimeChecker() {
        return queueTimeChecker;
    }

    @Override
    public void update(Observable o, Object arg) {
        queueObservable.notifyObservers(arg);
    }

    public class QueueObservable extends Observable {
        @Override
        public void notifyObservers(Object arg) {
            setChanged();
            super.notifyObservers(arg);
        }
    }

    public void addObserver(Observer observer) {
        queueObservable.addObserver(observer);
    }
}
