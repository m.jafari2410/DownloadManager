/**
 * This is class download and does downloads.
 *
 * @author Mohammad Jafari
 * @version 1.0
 * @since 6.4.2018
 */

import com.sun.org.apache.bcel.internal.generic.IF_ACMPEQ;
import javafx.animation.PauseTransition;

import javax.swing.*;
import java.awt.*;
import java.net.*;
import java.io.*;

public class Download implements Runnable, Serializable {
    private URL url;
    private HttpURLConnection connection;
    private File file;
    private long fileLength;
    private String fileName;
    private String disposition;
    private DownloadInfo downloadInfo;

    public Download(DownloadInfo downloadInfo) {
        this.downloadInfo = downloadInfo;
    }

    public Boolean connect() {
        try {
            url = new URL(downloadInfo.getLink());
        } catch (MalformedURLException e) {
            System.out.println(downloadInfo.getLink());
            return false;
        }
        if ("http".equals(url.getProtocol())) {
            try {
                connection = (HttpURLConnection) url.openConnection();
                connection.connect();
                disposition = connection.getHeaderField("Content-Disposition");
                String contentType = connection.getContentType();
                System.out.println(contentType);
                if (connection.getResponseCode() / 100 == 2) {
                    fileLength = connection.getContentLengthLong();
                    downloadInfo.setSize(fileLength);
                    downloadInfo.setName(getFileName(downloadInfo.getLink()));
                    return true;
                } else
                    return false;
            } catch (IOException e) {
                System.err.println();
                return false;
            }
        } else {
            return false;
        }
    }

    @Override
    public void run() {
        while (DownloadManager.currentNumOfDownloads >= Integer.valueOf(DownloadManager.limitDownloadNumber) || downloadInfo.isWaiting()){
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        DownloadManager.currentNumOfDownloads++;
        long totalRead = downloadInfo.getDownloadedSize();
        int count = 0;
        Boolean isCanceled = false;
        byte[] buffer = new byte[1024];
        file = new File(downloadInfo.getStorageAddress() + "/" + getFileName());
        InputStream inputStream;
        try (RandomAccessFile outputStream = new RandomAccessFile(file, "rw")) {
            inputStream = connection.getInputStream();
            inputStream.skip(totalRead);
            outputStream.seek(totalRead);
            while (totalRead < fileLength && !downloadInfo.isCancelled() && !downloadInfo.isPaused() && (count = inputStream.read(buffer)) != -1) {
                outputStream.write(buffer, 0, count);
                totalRead += count;
                downloadInfo.setDownloadedSize(totalRead);
            }
            inputStream.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        DownloadManager.currentNumOfDownloads--;
        if (downloadInfo.getDownloadedSize() == fileLength) {
            downloadInfo.setFinished(true);
            downloadInfo.getFinishTime();
            if (downloadInfo.isLunch()) {
                try {
                    Desktop.getDesktop().open(file);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    public String getFileName(String link) {
        if (disposition != null) {
            int index = disposition.indexOf("filename=");
            if (index > 0)
                fileName = disposition.substring(index + 10, disposition.length() - 1);
        } else {
            fileName = link.substring(link.lastIndexOf("/") + 1, link.length());
        }
        return fileName;
    }

    public long getFileLength() {
        return fileLength;
    }

    public String getFileName() {
        return fileName;
    }

}
