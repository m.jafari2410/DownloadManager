/**
 * This method models any download with all it's specifics.
 *
 * @author Mohammad Jafari
 * @version 1.0
 * @since 5.9.2018
 */

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class DownloadInfo implements Serializable {
    private String name;
    private String link;
    private String storageAddress;
    private long length ;
    private long downloadedSize;
    private String startTime;
    private String finishTime;
    private Boolean isWaiting;
    private Boolean isFinished = false;
    private String timeFinished;
    private float percent;
    private Boolean isCancelled = false;
    private Boolean isPaused = false;
    private Boolean lunch = false;


    public Boolean isCancelled() {
        return isCancelled;
    }

    public Boolean isPaused() {
        return isPaused;
    }

    public void setCancelled(Boolean cancelled) {
        isCancelled = cancelled;
    }

    public void setPaused(Boolean paused) {
        isPaused = paused;
    }

    /**
     * This is constructor and initials the fields of the download file
     * @param name is name of the downloading file
     * @param link  is the url
     * @param storageAddress  is the storage address
     */
    public DownloadInfo(String name, String link, String storageAddress) {
        this.name = name;
        this.link = link;
        this.storageAddress = storageAddress;
        setStartTime();
    }

    /**
     * The constructor is overloaded here. File name has been removed from parameter list. This constructor is used in NewDownloadFrame.
     * @param link link is url
     * @param storageAddress storage address of the download.
     */
    public DownloadInfo(String link, String storageAddress) {
        this.link = link;
        this.storageAddress = storageAddress;
        setStartTime();
    }

    /**
     * this is a getter method and returns the storage address
     */
    public String getStorageAddress() {
        return storageAddress;
    }

    /**
     * this method is used to find out weather the download has been finished or not
     */
    public Boolean isWaiting() {
        return isWaiting;
    }

    /**
     * This method sets the download to be waited.
     * @param waiting
     */
    public void setWaiting(Boolean waiting) {
        isWaiting = waiting;
    }

    /**
     * this is a getter method and returns the name of the downloading file
     */
    public String getName() {
        return name;
    }


    public void determineSize() {
        //here length of file will be determined according to the link
    }

    /**
     * This method sets the start time
     */
    public void setStartTime() {
        startTime = LocalDateTime.now().getYear() + "-" + LocalDateTime.now().getMonthValue() + "-" + LocalDateTime.now().getDayOfMonth() + " " + LocalDateTime.now().getHour() + ":" + LocalDateTime.now().getMinute() + ":" + LocalDateTime.now().getSecond();
    }

    /**
     * this is a getter method and returns the start time
     */
    public String getStartTime() {
        return startTime;
    }

    public String getFinishTime() {
        setFinishTime();
        return finishTime;
    }

    public void setFinishTime(){
        finishTime = LocalDateTime.now().getYear() + "-" + LocalDateTime.now().getMonthValue() + "-" + LocalDateTime.now().getDayOfMonth() + " " + LocalDateTime.now().getHour() + ":" + LocalDateTime.now().getMinute() + ":" + LocalDateTime.now().getSecond();
    }

    /**
     * this is a getter method and returns the length of the downloading file
     */
    public long getSize() {
        return length;
    }

    /**
     * this is a getter method and returns the downloaded length of the downloading file
     */
    public long getDownloadedSize() {
        return downloadedSize;
    }

    /**
     * This is a getter method and gets the download link
     */
    public String getLink() {
        return link;
    }

    /**
     * This method gets a boolean showing weather the download has been finished or not.
     */
    public Boolean isFinished() {
        return isFinished;
    }


    public void setFinished(Boolean finished) {
        isFinished = finished;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setSize(long length) {
        this.length = length / 1000;
    }

    public void setDownloadedSize(long downloadedSize) {
        this.downloadedSize = downloadedSize;
    }

    public float getPercent() {
            percent = (float) (((float)downloadedSize / ((float) length * 1000)) * 100f);
        return percent;
    }

    public Boolean isLunch() {
        return lunch;
    }

    public void setLunch(Boolean lunch) {
        this.lunch = lunch;
    }
}
