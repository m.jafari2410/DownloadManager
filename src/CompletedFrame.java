/**
 * This class is used to show completed downloads in completed downloads part of the program.
 * And also it extends class showList and implements the interface Observer to implement design pattern of observer
 * for updating list of downloads.
 *
 * @author Mohammad Jafari
 * @version 1.0
 * @since 5.9.2018
 */

import javax.swing.*;
import java.awt.*;
import java.io.IOException;
import java.util.Observable;
import java.util.Observer;

public class CompletedFrame extends ShowList implements Observer {

    /**
     * This is constructor and adds this class to the center panel
     */
    public CompletedFrame() {
        super();
        infoObservable = new InfoObservable();
        update();
        this.add(floatPanel);
    }

    /**
     * Method update of class ShowList is overridden here
     * this method added completed downloads to the floatPanel of class ShowList
     */
    @Override
    public void update() {
        int i = 0;
        i = DownloadManager.completedDownloads.size();
        floatPanel = new JPanel(new FlowLayout());
        floatPanel.setBackground(Color.white);
        for (DownloadInfo downloadInfo : DownloadManager.completedDownloads) {
            CompletedItemFrame completedItemFrame = new CompletedItemFrame(downloadInfo);
            completedItemFrame.addObserver(this);
            floatPanel.add(completedItemFrame);
        }
    }

    /**
     * This method of interface of Observable is implemented here to update the list of completed downloads when
     * list of completed downloads is changed or when namePanel of class CompletedItemFrame is clicked by right
     * click.
     */
    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof DownloadInfo) {
            infoObservable.notifyObservers(arg);
            return;
        }
        if (!(arg instanceof CompletedItemFrame))
            return;

        CompletedItemFrame completedItemFrame = (CompletedItemFrame) arg;
        this.floatPanel.remove(completedItemFrame);
        this.revalidate();
        this.validate();
        this.repaint();

    }
}
