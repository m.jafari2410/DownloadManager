/**
 * This is an interface of Maintainable which extends interface Serializable.
 *
 * @author Mohammad Jafari
 * @version 1
 * @since 5.30.2018
 */

import java.io.Serializable;

public interface Maintainable extends Serializable {
    /**
     * This method gets the current state of the program
     */
    public void get();

    /**
     * This method sets the state of the program
     */
    public void set();

    /**
     * This method save the current state of the program to folder : /jdm files
     */
    public void save();

    /**
     * This method load a file containing saved state of the program
     */
    public void load();
}
