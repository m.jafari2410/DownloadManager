/**
 * This class is used to maintain removed.jdm file which contains removed download items by user.
 * This class also implements interface Maintainable and Serializable.
 *
 * @author Mohammad Jafari
 * @version 1.0
 * @since 5.30.2018
 */

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

public class RemovedListMaintain implements Maintainable, Serializable {
    //list of removed downloads
    private ArrayList<DownloadInfo> removedList;

    /**
     * This method gets the current removed list
     */
    @Override
    public void get() {
        this.removedList = DownloadManager.removedDownloads;
    }

    /**
     * This method sets list of removed downloads
      */
    @Override
    public void set() {
        DownloadManager.removedDownloads = this.removedList;
    }

    /**
     * This method saves list of removed downloads to removed.jdm
     */
    @Override
    public void save() {
        get();
        File fileOutputAddress = new File(DownloadManager.saveAddress + "/jdm Files");
        if (!fileOutputAddress.exists())
            fileOutputAddress.mkdir();
        System.gc();
        try {
            Files.deleteIfExists(Paths.get(DownloadManager.saveAddress + "/jdm Files/removed.jdm"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(fileOutputAddress + "/removed.jdm"))) {
            objectOutputStream.writeObject(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method loads list of removed downloads from removed.jdm
     */
    @Override
    public void load() {
        if (!new File(DownloadManager.saveAddress + "/jdm Files/remove.jdm").exists())
            return;
        File fileInputAddress = new File(DownloadManager.saveAddress + "/jdm Files");
        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(fileInputAddress + "/removed.jdm"))) {
            RemovedListMaintain removedListMaintain = (RemovedListMaintain) objectInputStream.readObject();
            this.removedList = removedListMaintain.removedList;
            set();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
