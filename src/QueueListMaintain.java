/**
 * This class is used to maintain queue.jdm file which contains list of queues and also download list of each queue.
 * This class also implements interface Maintainable and Serializable.
 *
 * @author Mohammad Jafari
 * @version 3.0
 * @since 5.30.2018
 */

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

public class QueueListMaintain implements Maintainable, Serializable {
    //list of queues
    public ArrayList<Queue> queues;
    //list of name of queues
    public ArrayList<String> queuesNames;

    /**
     * This method gets lists of queues from class DownloadManager
     */
    @Override
    public void get() {
        this.queues = DownloadManager.queues;
        this.queuesNames = DownloadManager.queuesNames;
    }

    /**
     * This method sets lists of queues of class DownloadManager
     */
    @Override
    public void set() {
        DownloadManager.queues = this.queues;
        DownloadManager.queuesNames = this.queuesNames;
    }

    /**
     * This method uses serialization to write object of this class which contains lists of queues to path of jdm Files
     */
    @Override
    public void save() {
        get();
        File fileOutputAddress = new File(DownloadManager.saveAddress + "/jdm Files");
        if (!fileOutputAddress.exists())
            fileOutputAddress.mkdir();

        System.gc();
        try {
            Files.deleteIfExists(Paths.get(DownloadManager.saveAddress + "/jdm Files/queue.jdm"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(fileOutputAddress + "/queue.jdm"))) {
            objectOutputStream.writeObject(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method uses serialization to load object if this class which contains lists of queues from path of jdm Files.
     */
    @Override
    public void load() {
        if (!new File(DownloadManager.saveAddress + "/jdm Files/queue.jdm").exists())
            return;

        File fileInputAddress = new File(DownloadManager.saveAddress + "/jdm Files");
        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(fileInputAddress + "/queue.jdm"))) {
            QueueListMaintain queueListMaintain = (QueueListMaintain) objectInputStream.readObject();
            this.queues = queueListMaintain.queues;
            this.queuesNames = queueListMaintain.queuesNames;
            set();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
