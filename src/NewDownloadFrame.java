/**
 * This class creates a JFrame for creating new downloads.
 *
 * @author Mohammad Jafari
 * @version 1.0
 * @since 5.9.2018
 */

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.filechooser.FileSystemView;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.Executor;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class NewDownloadFrame extends JFrame implements ActionListener {
    //if the file is valid
    private Boolean isThere = false;
    private DownloadInfo downloadInfo;
    private JTextField linkField;
    private JTextField nameField;
    private JTextField addressField;
    //size of the file
    private long fileSize;
    //modes of download
    private JRadioButton autoMode;
    private JRadioButton manualMode;
    //for choosing the queue
    private JSpinner queueSpinner;
    private SpinnerListModel queueList;
    private DownloadListObservable observable;
    private JLabel nameLabel;
    private JPanel mainPanel;
    private static ExecutorService pool = Executors.newCachedThreadPool();
    private JCheckBox lunchCheck;


    public NewDownloadFrame() {
        super("New Download");

        observable = new DownloadListObservable();

        JPanel contentPane = new JPanel(new BorderLayout(5, 5));
        contentPane.setBorder(new EmptyBorder(20, 10, 30, 10));
        this.setContentPane(contentPane);
        this.getContentPane().setBackground(Color.WHITE);

        JLabel linkLabel = new JLabel("Link:");
        linkField = new JTextField();
        linkField.setPreferredSize(new Dimension(350, linkLabel.getPreferredSize().height + 7));
        linkField.addActionListener(this);
        JPanel linkPanel = new JPanel(new BorderLayout(5, 5));
        linkPanel.add(linkLabel, BorderLayout.WEST);
        linkPanel.add(linkField, BorderLayout.EAST);
        linkPanel.setBackground(Color.white);

        nameLabel = new JLabel("File name: " + "(" + fileSize + "MB" + ")");
        nameField = new JTextField();
        nameField.setPreferredSize(new Dimension(350, nameLabel.getPreferredSize().height + 7));
        nameField.setEditable(false);
        JPanel namePanel = new JPanel(new BorderLayout(5, 5));
        namePanel.add(nameLabel, BorderLayout.WEST);
        namePanel.add(nameField, BorderLayout.EAST);
        namePanel.setBackground(Color.white);

        JLabel addressLabel = new JLabel("Store Path:");
        addressField = new JTextField(DownloadManager.saveAddress);
        addressField.setBackground(Color.white);
        addressField.setPreferredSize(new Dimension(350, addressLabel.getPreferredSize().height + 7));
        addressField.setCaretColor(Color.BLUE);
        addressField.addActionListener(this);
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        File defaultPath = new File(DownloadManager.saveAddress);
        if (!defaultPath.exists()) {
            defaultPath.mkdir();
        }
        addressField.setText(DownloadManager.saveAddress);
        fileChooser.setCurrentDirectory(new File(DownloadManager.saveAddress));
        JButton pathButton = new JButton("Browse");
        //if user clicked on the button set the address
        pathButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int returnValue = fileChooser.showOpenDialog(NewDownloadFrame.this);
                if (returnValue == JFileChooser.APPROVE_OPTION) {
                    File file = fileChooser.getSelectedFile();
                    addressField.setText(file.getAbsolutePath());
                }
            }
        });
        //if user entered in the address field
        addressField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fileChooser.setCurrentDirectory(new File(addressField.getText()));
            }
        });
        JPanel addressPanel = new JPanel(new BorderLayout(5, 5));
        addressPanel.add(pathButton, BorderLayout.EAST);
        addressPanel.add(addressLabel, BorderLayout.WEST);
        addressPanel.add(addressField, BorderLayout.CENTER);
        addressPanel.setBackground(Color.white);

        JPanel northPanel = new JPanel(new GridLayout(3, 1, 10, 40));
        northPanel.setBackground(Color.white);
        northPanel.add(linkPanel);
        northPanel.add(namePanel);
        northPanel.add(addressPanel);

        lunchCheck = new JCheckBox();
        lunchCheck.setBackground(Color.white);
        JLabel lunchLabel = new JLabel("Lunch Download when it is Completed");
        linkLabel.setBackground(Color.white);
        JPanel lunchPanel = new JPanel(new BorderLayout(5, 5));
        lunchPanel.setBackground(Color.white);
        lunchPanel.add(lunchLabel, BorderLayout.CENTER);
        lunchPanel.add(lunchCheck, BorderLayout.WEST);

        JLabel startMode = new JLabel("Start with:");
        autoMode = new JRadioButton("Automatically");
        autoMode.setSelected(true);
        manualMode = new JRadioButton("Queues");
        autoMode.setBackground(Color.pink);
        manualMode.setBackground(Color.pink);
        ButtonGroup buttonGroup = new ButtonGroup();
        buttonGroup.add(autoMode);
        buttonGroup.add(manualMode);
        if (!DownloadManager.queuesNames.isEmpty())
            queueList = new SpinnerListModel(DownloadManager.queuesNames);
        else
            queueList = new SpinnerListModel();
        queueSpinner = new JSpinner(queueList);
        queueSpinner.setEnabled(false);
        //selecting manual mode and the the queue
        manualMode.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                queueSpinner.setEnabled(true);
            }
        });
        //selecting the auto donwload
        autoMode.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                queueSpinner.setEnabled(false);
            }
        });
        JPanel manualModePanel = new JPanel(new BorderLayout(5, 5));
        manualModePanel.add(manualMode, BorderLayout.CENTER);
        manualModePanel.add(queueSpinner, BorderLayout.EAST);
        JPanel southPanel = new JPanel(new GridLayout(4, 1));
        southPanel.add(lunchPanel);
        southPanel.add(startMode);
        southPanel.add(autoMode);
        southPanel.add(manualModePanel);
        southPanel.setBackground(Color.white);

        JButton closeButton = new JButton("Cancel");
        JButton okButton = new JButton("Ok");
        okButton.addActionListener(this);
        //if close button is pressed
        closeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                NewDownloadFrame.super.setVisible(false);
            }
        });
        JPanel lastPanel = new JPanel(new GridLayout(1, 2));
        lastPanel.add(closeButton);
        lastPanel.add(okButton);
        lastPanel.setBackground(Color.white);

        mainPanel = new JPanel(new BorderLayout(5, 5));
        mainPanel.add(northPanel, BorderLayout.NORTH);
        mainPanel.add(southPanel, BorderLayout.CENTER);
        mainPanel.setBackground(Color.white);

        this.add(mainPanel, BorderLayout.NORTH);
        this.add(lastPanel, BorderLayout.EAST);
        this.pack();
        this.setVisible(true);
        this.setLocationRelativeTo(new ProcessingFrame());

    }


    /**
     * getting information of downloads from the user
     */
    public void setInfo() {
        if (autoMode.isSelected()) {
            DownloadManager.autoDownloads.add(downloadInfo);
            downloadInfo.setWaiting(false);
            if (lunchCheck.isSelected())
                downloadInfo.setLunch(true);
        } else {
            if (!DownloadManager.queues.get(DownloadManager.queuesNames.indexOf(queueSpinner.getValue())).isScheduling()) {
                downloadInfo.setWaiting(false);
            }
            else {
                downloadInfo.setWaiting(true);
            }
            DownloadManager.queues.get(DownloadManager.queuesNames.indexOf(queueSpinner.getValue())).add(downloadInfo);
            if (lunchCheck.isSelected()) {
                downloadInfo.setLunch(true);
            }
        }
    }

    /**
     * If the file is valid, calls the setInfo method
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        downloadInfo = new DownloadInfo(linkField.getText(), addressField.getText());
        Download newDownload = new Download(downloadInfo);
        if (newDownload.connect()) {
            setInfo();
            pool.execute(newDownload);
            if (autoMode.isSelected())
                observable.notifyObservers("update downloads");
            else
                observable.notifyObservers(DownloadManager.queues.get(DownloadManager.queuesNames.indexOf(queueSpinner.getValue())));
            this.setVisible(false);

        } else
            linkField.setText("Not valid");
    }


    /**
     * This class is used to notify observers (i.e class DMFrame) of changing download list
     *
     * @author Mohammad Jafari
     * @version 1.0
     * @since 5.9.2018
     */
    public class DownloadListObservable extends Observable {
        @Override
        public void notifyObservers(Object arg) {
            setChanged();
            super.notifyObservers(arg);
        }
    }

    //overriding method of class CompletedItemFrame
    public void addObserver(Observer observer) {
        observable.addObserver(observer);
    }
}
