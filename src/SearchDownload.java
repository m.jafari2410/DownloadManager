/**
 * This class models a searcher engine which goes through the auto downloads, completed downloads and also
 * all queues.
 *
 * @author Mohammad Jafari
 * @since  5.31.2018
 * @version 1.0
 */

import java.util.ArrayList;

public class SearchDownload {
    //list of matched downloads
    private ArrayList<DownloadInfo> foundDownloads;

    /**
     * This method goes through all sort of downloads and finds matches downloads.
     * @param wanted Is the word which we must find downloads with this name or containing this link
     * @return  list of matched downloads.
     */
    public ArrayList<DownloadInfo> findDownloads(String wanted) {
        foundDownloads = new ArrayList<DownloadInfo>();
        for (DownloadInfo downloadInfo : DownloadManager.autoDownloads) {
            if (downloadInfo.getName().contains(wanted)) {
                foundDownloads.add(downloadInfo);
                continue;
            } else if (downloadInfo.getLink().contains(wanted)) {
                foundDownloads.add(downloadInfo);
                continue;
            }
        }
        for (DownloadInfo downloadInfo : DownloadManager.completedDownloads) {
            if (downloadInfo.getName().contains(wanted)) {
                foundDownloads.add(downloadInfo);
                continue;
            } else if (downloadInfo.getLink().contains(wanted)) {
                foundDownloads.add(downloadInfo);
                continue;
            }
        }
        for (Queue queue : DownloadManager.queues) {
            for (DownloadInfo downloadInfo : queue.getDownloads()) {
                if (downloadInfo.getName().contains(wanted)) {
                    foundDownloads.add(downloadInfo);
                    continue;
                } else if (downloadInfo.getLink().contains(wanted)) {
                    foundDownloads.add(downloadInfo);
                    continue;
                }
            }
        }
        return foundDownloads;
    }
}
