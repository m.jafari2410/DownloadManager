/**
 * This class is used to maintain setting.jdm file which contains settings of the program such as look and feel and also storage path.
 * This class also implements interface Maintainable and Serializable.
 *
 * @author Mohammad Jafari
 * @version 1.0
 * @since 5.30.2018
 */
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class SettingMaintain implements Maintainable,Serializable {
    //limit number for concurrent downloads
    private String limitDownloadNumber;
    //storage address of the download
    private String saveAddress;
    //look and feel of the UI
    private String lookAndFeel;

    /**
     *This method gets the current setting values
     */
    @Override
    public void get() {
        this.limitDownloadNumber = DownloadManager.limitDownloadNumber;
        this.saveAddress = DownloadManager.saveAddress;
        this.lookAndFeel = Setting.getLookAndFeel();
    }

    /**
     * This method sets the setting of the program if a new load from outside file has done.
     */
    @Override
    public void set() {
        DownloadManager.limitDownloadNumber = this.limitDownloadNumber;
        DownloadManager.saveAddress = this.saveAddress;
        Setting.setLookAndFeel(this.lookAndFeel);
    }

    /**
     * This method saves the current state of program settings to /jdm files/setting.jdm
     */
    @Override
    public void save() {
        get();
        File fileOutputAddress = new File(DownloadManager.saveAddress + "/jdm Files");
        if (!fileOutputAddress.exists())
            fileOutputAddress.mkdir();
        System.gc();
        try {
            Files.deleteIfExists(Paths.get(DownloadManager.saveAddress + "/jdm Files/setting.jdm"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(fileOutputAddress + "/setting.jdm"))) {
            objectOutputStream.writeObject(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method load state of the program setting from a file in /jdm Files and named setting.jdm
     */
    @Override
    public void load() {
        if (!new File(DownloadManager.saveAddress + "/jdm Files/setting.jdm").exists()) {
            Setting.setLookAndFeel("Nimbus");
            return;
        }
        File fileInputAddress = new File(DownloadManager.saveAddress + "/jdm Files");
            try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(fileInputAddress + "/setting.jdm"))) {
                SettingMaintain settingMaintain = (SettingMaintain) objectInputStream.readObject();
                this.limitDownloadNumber = settingMaintain.limitDownloadNumber;
                this.saveAddress = settingMaintain.saveAddress;
                this.lookAndFeel = settingMaintain.lookAndFeel;
                set();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
    }
}
