/**
 * This class creates a new JFrame for settings of the program including limit number of download,
 * setting different look and feels and also setting the storage address.
 *
 * @author Mohammad Jafari
 * @version 1.0
 * @since 5.9.2018
 */

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.*;
import java.io.File;

public class Setting extends JFrame {
    private static String lookAndFeel;

    public Setting() {
        super("Setting");
        this.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);

        //setting the contentPane
        JPanel contentPane = new JPanel(new BorderLayout(5, 5));
        contentPane.setBorder(new EmptyBorder(35, 10, 20, 10));
        this.setContentPane(contentPane);
        this.getContentPane().setBackground(Color.white);
        this.setResizable(false);

        //setting limit number of download using JSpinner
        JLabel dLimitLabel = new JLabel("Limit number of concurrent downloads :");
        String limitNumber[] = new String[6];
        for (int i = 0; i <= 4; i++) {
            limitNumber[i] = (i + 1) + "";
        }
        limitNumber[5] = "infinite";
        SpinnerListModel limitNumberList = new SpinnerListModel(limitNumber);
        JSpinner limitSpinner = new JSpinner(limitNumberList);
        limitSpinner.setValue(DownloadManager.limitDownloadNumber);
        limitSpinner.setPreferredSize(new Dimension(80, dLimitLabel.getPreferredSize().height + 6));
        JPanel limitPanel = new JPanel(new FlowLayout());
        limitPanel.add(dLimitLabel);
        limitPanel.add(limitSpinner);
        limitPanel.setBackground(Color.white);

        //setting the storage address using JFileChooser
        JLabel addressLabel = new JLabel("Store Path:");
        JTextField addressField = new JTextField(DownloadManager.saveAddress);
        File defaultPath = new File(DownloadManager.saveAddress);
        if (!defaultPath.exists()) {
            defaultPath.mkdir();
        }
        addressField.setBackground(Color.white);
        addressField.setPreferredSize(new Dimension(350, addressLabel.getPreferredSize().height + 7));
        addressField.setCaretColor(Color.BLUE);
        JFileChooser fileChooser = new JFileChooser();
        fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        fileChooser.setCurrentDirectory(new File(DownloadManager.saveAddress));
        JButton pathButton = new JButton("Browse");
        pathButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int returnValue = fileChooser.showOpenDialog(Setting.this);
                if (returnValue == JFileChooser.APPROVE_OPTION) {
                    File file = fileChooser.getSelectedFile();
                    addressField.setText(file.getAbsolutePath());
                    DownloadManager.saveAddress = file.getAbsolutePath();
                }
            }
        });
        JPanel addressPanel = new JPanel(new BorderLayout(5, 5));
        addressPanel.add(pathButton, BorderLayout.EAST);
        addressPanel.add(addressLabel, BorderLayout.WEST);
        addressPanel.add(addressField, BorderLayout.CENTER);
        addressPanel.setBackground(Color.white);

        //setting look and feel using JSpinner
        JLabel lkAndFlLabel = new JLabel("Select look and feel:");
        String lookAndFeels[] = new String[5];
        lookAndFeels[0] = "Metal";
        lookAndFeels[1] = "Nimbus";
        lookAndFeels[2] = "CDE/Motif";
        lookAndFeels[3] = "Window";
        lookAndFeels[4] = "Windows Classic";
        SpinnerListModel lkAndFlList = new SpinnerListModel(lookAndFeels);
        JSpinner looksAndFeelSpinner = new JSpinner(lkAndFlList);
        looksAndFeelSpinner.setValue(lookAndFeel);
        looksAndFeelSpinner.setPreferredSize(new Dimension(looksAndFeelSpinner.getPreferredSize().width + 70, looksAndFeelSpinner.getPreferredSize().height + 5));
        JPanel lookAndFeelPanel = new JPanel(new FlowLayout());
        lookAndFeelPanel.add(lkAndFlLabel);
        lookAndFeelPanel.add(looksAndFeelSpinner);
        lookAndFeelPanel.setBackground(Color.white);

        JPanel northPanel = new JPanel(new BorderLayout(5, 5));
        northPanel.add(limitPanel, BorderLayout.NORTH);
        northPanel.add(addressPanel, BorderLayout.CENTER);
        northPanel.setBackground(Color.white);

        JButton closeButton = new JButton("Cancel");
        JButton okButton = new JButton("Ok");
        JPanel lastPanel = new JPanel(new GridLayout(1, 2));
        lastPanel.add(closeButton);
        lastPanel.add(okButton);

        JPanel southPanel = new JPanel(new BorderLayout(5, 5));
        southPanel.add(lookAndFeelPanel, BorderLayout.WEST);
        southPanel.add(lastPanel, BorderLayout.EAST);
        southPanel.setBackground(Color.white);

        JPanel emptyPanel = new JPanel(new BorderLayout(5, 5));
        emptyPanel.setPreferredSize(new Dimension(400, 200));
        emptyPanel.setBackground(Color.white);

        JLabel filterLabel = new JLabel("Filter Sites:");

        JTextArea filterTxtArea = new JTextArea();
        filterTxtArea.setPreferredSize(new Dimension(400, 50));
        filterTxtArea.setLineWrap(true);
        filterTxtArea.setVisible(true);
        filterTxtArea.setBackground(Color.white);
        if (!DownloadManager.filteredSites.isEmpty())
            filterTxtArea.append(DownloadManager.filteredSites);
        filterTxtArea.setBackground(Color.getHSBColor(124,66,125));

        JButton filterButton = new JButton("apply");
        filterButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DownloadManager.filteredSites = filterTxtArea.getText();
                new FilterSitesMaintain().save();
            }
        });

        filterTxtArea.addKeyListener(new KeyAdapter() {
            @Override
            public void keyReleased(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    DownloadManager.filteredSites = filterTxtArea.getText();
                    new FilterSitesMaintain().save();
                    filterButton.requestFocus(true);
                }
            }
        });

        JScrollPane filterScroll = new JScrollPane(filterTxtArea);
        filterScroll.setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_ALWAYS);

        JPanel filterPanel = new JPanel(new FlowLayout());
        filterPanel.setBackground(Color.white);
        filterPanel.add(filterScroll);
        filterPanel.add(filterButton);

        emptyPanel.add(filterLabel, BorderLayout.NORTH);
        emptyPanel.add(filterPanel, BorderLayout.CENTER);

        //adding to the contentPane
        this.add(northPanel, BorderLayout.NORTH);
        this.add(emptyPanel, BorderLayout.CENTER);
        this.add(southPanel, BorderLayout.SOUTH);

        this.setLocationRelativeTo(new ProcessingFrame());

        //if user entered in the spinnerField of the limitSpinner
        final JTextField spinnerField = ((JSpinner.DefaultEditor) limitSpinner.getEditor()).getTextField();
        spinnerField.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    DownloadManager.limitDownloadNumber = (String) limitSpinner.getValue();
                    lookAndFeel = (String) looksAndFeelSpinner.getValue();
                    DownloadManager.saveAddress = addressField.getText();
                    changeUI();
                    Setting.super.setVisible(false);
                }

            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });

        //if user entered in addressField
        addressField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fileChooser.setCurrentDirectory(new File(addressField.getText()));
                DownloadManager.limitDownloadNumber = (String) limitSpinner.getValue();
                lookAndFeel = (String) looksAndFeelSpinner.getValue();
                DownloadManager.saveAddress = addressField.getText();
                changeUI();
                Setting.super.setVisible(false);
            }
        });

        //if user closed
        closeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Setting.super.setVisible(false);
            }
        });

        //if user clicked on the ok button
        okButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                DownloadManager.limitDownloadNumber = (String) limitSpinner.getValue();
                lookAndFeel = (String) looksAndFeelSpinner.getValue();
                DownloadManager.saveAddress = addressField.getText();
                changeUI();
                Setting.super.setVisible(false);
            }
        });

        //if user entered in the spinnerField of the lookAndFeelSpinner
        final JTextField lookAndFeelField = ((JSpinner.DefaultEditor) looksAndFeelSpinner.getEditor()).getTextField();
        lookAndFeelField.addKeyListener(new KeyListener() {
            @Override
            public void keyTyped(KeyEvent e) {

            }

            @Override
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                    DownloadManager.limitDownloadNumber = (String) limitSpinner.getValue();
                    lookAndFeel = (String) looksAndFeelSpinner.getValue();
                    DownloadManager.saveAddress = addressField.getText();
                    changeUI();
                    Setting.super.setVisible(false);
                }
            }

            @Override
            public void keyReleased(KeyEvent e) {

            }
        });

        this.pack();
        this.setVisible(true);
    }

    public void changeUI() {
        try {
            Main.uiSet();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }

    public static String getLookAndFeel() {
        return lookAndFeel;
    }

    public static void setLookAndFeel(String lookAndFeel) {
        Setting.lookAndFeel = lookAndFeel;
    }
}
