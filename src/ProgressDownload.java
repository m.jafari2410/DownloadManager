import javax.swing.*;
import java.util.List;
import java.util.concurrent.ExecutionException;

public class ProgressDownload extends SwingWorker<Boolean, Void> {
    private DownloadInfo downloadInfo;
    private DownloadingItemFrame downloadingItemFrame;
    private ShowList showList;

    public ProgressDownload(DownloadInfo downloadInfo, DownloadingItemFrame downloadingItemFrame, ShowList showList) {
        this.downloadInfo = downloadInfo;
        this.downloadingItemFrame = downloadingItemFrame;
        this.showList = showList;
        if (showList instanceof ProcessingFrame)
            this.showList = (ProcessingFrame) showList;
        else if (showList instanceof QueuesFrame)
            this.showList = (QueuesFrame) showList;
    }

    @Override
    protected Boolean doInBackground() throws Exception {
        float progress = downloadInfo.getPercent();
        while (progress < 100 && !isCancelled() && !downloadInfo.isPaused()) {
            progress = downloadInfo.getPercent();
            downloadingItemFrame.setProgressBar((int) progress);
            downloadingItemFrame.setRelativeDownloadSize(String.format("%dKb / %dKb", downloadInfo.getDownloadedSize() / 1000, downloadInfo.getSize()));
            setProgress((int) progress);
        }
        progress = downloadInfo.getPercent();
        if (progress >= 100)
            return true;
        else
            return false;
    }

    @Override
    protected void done() {
        try {
            if (get()) {
                DownloadManager.completedDownloads.add(downloadInfo);
                if (!DownloadManager.autoDownloads.remove(downloadInfo))
                    for (Queue queue : DownloadManager.queues)
                        queue.getDownloads().remove(downloadInfo);

                showList.update();
                if (showList instanceof QueuesFrame)
                    showList.getInfoObservable().notifyObservers(((QueuesFrame) showList).getQueue());
                else
                    showList.getInfoObservable().notifyObservers("update downloads");
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }
    }
}
