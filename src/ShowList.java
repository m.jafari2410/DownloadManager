/**
 * This class is used to show list of the now downloading, queue downloads or completed downloads.
 *
 * @author Mohammad Jafari
 * @version 1.0
 * @since 5.9.2018
 */

import javax.swing.*;
import java.awt.*;
import java.util.Observable;
import java.util.Observer;

public class ShowList extends JPanel {
    protected static int i;
    protected JPanel floatPanel;
    protected InfoObservable infoObservable;

    public ShowList() {
        super(new GridLayout(i, 1));
        JMenuBar menuBar = new JMenuBar();
        this.setBackground(Color.white);
        this.removeAll();
    }

    public void update() {
    }

    public class InfoObservable extends Observable {
        @Override
        public void notifyObservers(Object arg) {
            setChanged();
            super.notifyObservers(arg);
        }

    }

    public InfoObservable getInfoObservable() {
        return infoObservable;
    }

    /**
     * This method is for adding observer to observables of class InfoObservable
     *
     * @param observer is an object of class InfoObservable
     * @return this class as a ProcessingFrame
     */
    public ShowList addObserver(Observer observer) {
        infoObservable.addObserver(observer);
        return this;
    }
}

