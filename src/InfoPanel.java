/**
 * This panel gives some information about the download file if user right click on the name of the download file.
 *
 * @author Mohammad Jafari
 * @version 1.0
 * @since 5.9.2018
 */

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

public class InfoPanel extends JPanel {
    public InfoPanel(DownloadInfo downloadInfo) {
        this.setLayout(new GridLayout(7,1,5,40));
        this.setBackground(Color.white);

        JPanel namePanel = new JPanel(new BorderLayout(5, 5));
        JLabel fileLabel = new JLabel("File:");
        JLabel nameLabel = new JLabel(downloadInfo.getName());
        namePanel.add(fileLabel, BorderLayout.WEST);
        namePanel.add(nameLabel, BorderLayout.CENTER);
        namePanel.setBackground(Color.white);

        JPanel statusPanel = new JPanel(new BorderLayout(5, 5));
        JLabel statusLabel = new JLabel("Status:");
        JLabel statusLabel2 = new JLabel(downloadInfo.isFinished() ? "Completed" : "Uncompleted");
        statusPanel.add(statusLabel, BorderLayout.WEST);
        statusPanel.add(statusLabel2, BorderLayout.CENTER);
        statusPanel.setBackground(Color.white);

        JPanel sizePanel = new JPanel(new BorderLayout(5, 5));
        JLabel sizeLabel = new JLabel("Size:");
        JLabel sizeLabel2 = new JLabel(downloadInfo.getSize() + "");
        sizePanel.add(sizeLabel, BorderLayout.WEST);
        sizePanel.add(sizeLabel2, BorderLayout.CENTER);
        sizePanel.setBackground(Color.white);

        JPanel savedPanel = new JPanel(new BorderLayout(5, 5));
        JLabel savedLabel = new JLabel("Saved to:");
        JLabel savedLabel1 = new JLabel(downloadInfo.getStorageAddress());
        savedPanel.add(savedLabel, BorderLayout.WEST);
        savedPanel.add(savedLabel1, BorderLayout.CENTER);
        savedPanel.setBackground(Color.white);

        JPanel createTimePanel = new JPanel(new BorderLayout(5, 5));
        JLabel createTimeLabel = new JLabel("Created:");
        JLabel createTimeLabel2 = new JLabel(downloadInfo.getStartTime());
        createTimePanel.add(createTimeLabel, BorderLayout.WEST);
        createTimePanel.add(createTimeLabel2, BorderLayout.CENTER);
        createTimePanel.setBackground(Color.white);

        JPanel finishedTimePanel = new JPanel(new BorderLayout(5, 5));
        JLabel finishedTimeLabel = new JLabel("Finished:");
        JLabel finishedTimeLabel2 = new JLabel(downloadInfo.getFinishTime());
        finishedTimePanel.add(finishedTimeLabel, BorderLayout.WEST);
        finishedTimePanel.add(finishedTimeLabel2, BorderLayout.CENTER);
        finishedTimePanel.setBackground(Color.white);

        JPanel urlPanel = new JPanel(new BorderLayout(5, 5));
        String linkStr = downloadInfo.getLink();
        String link1 = linkStr.substring(0,linkStr.length()/2);
        JLabel urlLabel = new JLabel("URL:" + link1);
        String link2 = linkStr.substring(linkStr.length()/2,linkStr.length() - 1);
        JLabel urlLabel2 = new JLabel( link2);
        urlPanel.add(urlLabel, BorderLayout.NORTH);
        urlPanel.add(urlLabel2, BorderLayout.CENTER);
        urlPanel.setBackground(Color.white);

        JPanel hiddenPanel = new JPanel(new BorderLayout(5, 5));
        hiddenPanel.setBackground(Color.white);
        ImageIcon imageIcon = new ImageIcon("Images/b.png");
        JButton hiddenButton = new JButton(imageIcon);
        hiddenButton.setBackground(Color.white);
        hiddenPanel.add(hiddenButton, BorderLayout.EAST);
        //hidden button handling for hidden the panel
        hiddenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                showPanel(false);
            }
        });

        JPanel panel1 = new JPanel(new BorderLayout(5,40));
        panel1.add(namePanel,BorderLayout.NORTH);
        panel1.add(statusPanel,BorderLayout.SOUTH);
        panel1.setBackground(Color.white);

        JPanel panel2 = new JPanel(new BorderLayout(5,40));
        panel2.add(sizePanel,BorderLayout.NORTH);
        panel2.add(savedPanel,BorderLayout.SOUTH);
        panel2.setBackground(Color.white);

        JPanel panel3 = new JPanel(new BorderLayout(5,40));
        panel3.add(createTimePanel,BorderLayout.NORTH);
        panel3.add(finishedTimePanel,BorderLayout.SOUTH);
        panel3.setBackground(Color.white);

        JPanel panel4 = new JPanel(new BorderLayout(5,40));
        panel4.setBackground(Color.white);
        panel4.add(urlPanel,BorderLayout.NORTH);
        panel4.add(hiddenPanel,BorderLayout.SOUTH);

        this.add(panel1);
        this.add(panel2);
        this.add(panel3);
        this.add(urlPanel);
        this.add(hiddenPanel);

        showPanel(true);
    }

    /**
     * This method set and unset the visibility of the panel.
     */
    public void showPanel(Boolean bool) {
        this.setVisible(bool);
    }
}
