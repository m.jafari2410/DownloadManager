/**
 * This class implements interface of PropertyChangeListener and listens to changes that is done when downloading.
 *
 * @author Mohammad Jafari
 * @since 6.4.2018
 * @version 1.0
 */
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Observable;

public class ProgressListener implements PropertyChangeListener{
    private DownloadingItemFrame downloadingItemFrame;
    private Observable infoObservable;
    private Observable infoObservable1;
    private DownloadInfo downloadInfo;
    public ProgressListener (DownloadingItemFrame downloadingItemFrame, DownloadInfo downloadInfo){
        this.downloadingItemFrame = downloadingItemFrame;
        this.downloadInfo = downloadInfo;
    }
    @Override
    public void propertyChange(PropertyChangeEvent e) {
        if (e.getPropertyName().equals("progress"))
            downloadingItemFrame.setProgressBar((Integer) e.getNewValue());
    }
}
