/**
 * This class is used to maintain filter.jdm file which contains filtered sites names of the program such.
 * This class also implements interface Maintainable and Serializable.
 *
 * @author Mohammad Jafari
 * @version 1.0
 * @since 5.30.2018
 */

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FilterSitesMaintain implements Maintainable, Serializable {
    //filtered sites
    private String filteredSites;

    /**
     * This method gets the current filter sites
     */
    @Override
    public void get() {
        filteredSites = DownloadManager.filteredSites;
    }

    /**
     * This method sets the filter sites of the program
     */
    @Override
    public void set() {
        DownloadManager.filteredSites = filteredSites;
    }

    /**
     * This method saves the current state of program filter sites to /jdm files/filter.jdm
     */
    @Override
    public void save() {
        get();
        File fileOutputAddress = new File(DownloadManager.saveAddress + "/jdm Files");
        if (!fileOutputAddress.exists())
            fileOutputAddress.mkdir();
        System.gc();
        try {
            Files.deleteIfExists(Paths.get(DownloadManager.saveAddress + "/jdm Files/filter.jdm"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(fileOutputAddress + "/filter.jdm"))) {
            objectOutputStream.writeObject(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method load state of the program filter sites from a file in /jdm Files and named filter.jdm
     */
    @Override
    public void load() {
        if (!new File(DownloadManager.saveAddress + "/jdm Files/filter.jdm").exists())
            return;
        File fileInputAddress = new File(DownloadManager.saveAddress + "/jdm Files");
        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(fileInputAddress + "/filter.jdm"))) {
            FilterSitesMaintain filterSiteMaintain = (FilterSitesMaintain) objectInputStream.readObject();
            this.filteredSites = filterSiteMaintain.filteredSites;
            set();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
