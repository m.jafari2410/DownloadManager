/**
 * This class models download manager and handles network programmings of the program.
 *
 * @author Mohammad Jafari
 * @version 4.0
 * @since 5.9.2018
 */

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.Serializable;
import java.net.HttpURLConnection;
import java.util.ArrayList;

public class DownloadManager implements Serializable {
    //list of queues
    public static ArrayList<Queue> queues = new ArrayList<Queue>();
    //list of name of queues
    public static ArrayList<String> queuesNames = new ArrayList<String>();
    //list of downloads that are not in queues
    public static ArrayList<DownloadInfo> autoDownloads = new ArrayList<DownloadInfo>();
    //list of downloads which have been completed
    public static ArrayList<DownloadInfo> completedDownloads = new ArrayList<DownloadInfo>();
    public static String limitDownloadNumber = 3 + "";
    public static int currentNumOfDownloads = 0;
    public static String saveAddress = System.getProperty("user.home") + File.separator + "Purple Downloads";
    //list of removed downloads
    public static ArrayList<DownloadInfo> removedDownloads = new ArrayList<DownloadInfo>();
    //list of filtered sites
    public static String filteredSites = new String();
    //list of found download
    public static ArrayList<DownloadInfo> foundDownloads = new ArrayList<DownloadInfo>();

    public DownloadManager() {
    }

    public void download(DownloadInfo downloadInfo){
    }

    public void pause(){

    }

    public void resume(){

    }

    public void cancel(){

    }

}
