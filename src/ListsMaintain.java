/**
 * This class is used to maintain list.jdm file which contains list of downloads
 * This class also implements interface Maintainable and Serializable.
 *
 * @author Mohammad Jafari
 * @version 3.0
 * @since 5.30.2018
 */

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;

public class ListsMaintain implements Maintainable, Serializable {
    //list of downloads that are not in queues
    public ArrayList<DownloadInfo> autoDownloads;
    //list of downloads which have been completed
    public ArrayList<DownloadInfo> completedDownloads;

    /**
     * This method sets lists of downloads of class DownloadManager
     */
    @Override
    public void set() {
        DownloadManager.autoDownloads = this.autoDownloads;
        DownloadManager.completedDownloads = this.completedDownloads;
    }

    /**
     * This method gets lists of downloads from class DownloadManager
     */
    @Override
    public void get() {
        this.autoDownloads = DownloadManager.autoDownloads;
        this.completedDownloads = DownloadManager.completedDownloads;
    }

    /**
     * This method uses serialization to write object of this class which contains lists of downloads to path of jdm Files
     */
    @Override
    public void save() {
        get();
        File fileOutputAddress = new File(DownloadManager.saveAddress + "/jdm Files");
        if (!fileOutputAddress.exists())
            fileOutputAddress.mkdir();

        System.gc();
        try {
            Files.deleteIfExists(Paths.get(DownloadManager.saveAddress + "/jdm Files/list.jdm"));
        } catch (IOException e) {
            e.printStackTrace();
        }

        try (ObjectOutputStream objectOutputStream = new ObjectOutputStream(new FileOutputStream(fileOutputAddress + "/list.jdm"))) {
            objectOutputStream.writeObject(this);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method uses serialization to load object if this class which contains lists of downloads from path of jdm Files.
     */
    @Override
    public void load() {
        if (!new File(DownloadManager.saveAddress + "/jdm Files/queue.jdm").exists())
            return;
        File fileInputAddress = new File(DownloadManager.saveAddress + "/jdm Files");
        try (ObjectInputStream objectInputStream = new ObjectInputStream(new FileInputStream(fileInputAddress + "/list.jdm"))) {
            ListsMaintain listsMaintain = (ListsMaintain) objectInputStream.readObject();
            this.completedDownloads = listsMaintain.completedDownloads;
            this.autoDownloads = listsMaintain.autoDownloads;
            set();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
}
