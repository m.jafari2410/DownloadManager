/**
 * This class is used to show downloading files in processing downloads part of the program.
 * And also it extends class showList and implements the interface Observer to implement design pattern of observer
 * for updating list of downloads.
 *
 * @author Mohammad Jafari
 * @version 1.0
 * @since 5.9.2018
 */

import javax.swing.*;
import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Observable;
import java.util.Observer;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class ProcessingFrame extends ShowList implements Observer {
    private DownloadingItemFrame downloadingItemFrame;
    private DownloadInfo downloadInfo;
    private ProgressDownload progressDownload;

    /**
     * This is constructor and adds this panel to the floatPanel and at the end this would be the centerPanel of the class DMFrame
     */
    public ProcessingFrame() {
        super();
        infoObservable = new InfoObservable();
        update();
        this.add(floatPanel);
    }

    /**
     * This method is called from the super class constructor.
     * This method runs through the list of downloads and queues and finds the now processing downloads.
     */
    @Override
    public void update() {


        floatPanel = new JPanel(new FlowLayout());
        floatPanel.setBackground(Color.white);
        for (DownloadInfo downloadInfo : DownloadManager.autoDownloads) {
            this.downloadInfo = downloadInfo;
            downloadingItemFrame = new DownloadingItemFrame(downloadInfo);
            progressDownload = new ProgressDownload(downloadInfo, downloadingItemFrame,this);
            progressDownload.addPropertyChangeListener(new ProgressListener(downloadingItemFrame, downloadInfo));
            progressDownload.execute();
            downloadingItemFrame.addObserver(this);
            floatPanel.add(downloadingItemFrame);
        }

        for (Queue queue : DownloadManager.queues) {
            for (DownloadInfo downloadInfo : queue.getDownloads()) {
                if (!downloadInfo.isWaiting()) {
                    DownloadingItemFrame downloadingItemFrame = new DownloadingItemFrame(downloadInfo);
                    progressDownload = new ProgressDownload(downloadInfo, downloadingItemFrame,this);
                    progressDownload.addPropertyChangeListener(new ProgressListener(downloadingItemFrame, downloadInfo));
                    progressDownload.execute();
                    downloadingItemFrame.addObserver(this);
                    floatPanel.add(downloadingItemFrame);
                }
            }
        }
    }

    /**
     * This method of interface of Observable is implemented here to update the list of downloads when
     * list of now downloading files is changed or when namePanel of class DownloadingItemFrame is right clicked.
     */
    @Override
    public void update(Observable o, Object arg) {
        if (arg.equals("update")){
            update();
            infoObservable.notifyObservers("update downloads");
        }
        if (arg instanceof DownloadInfo) {
            infoObservable.notifyObservers(arg);
            return;
        }
        if (!(arg instanceof DownloadingItemFrame))
            return;
        DownloadingItemFrame downloadingItemFrame = (DownloadingItemFrame) arg;
        this.floatPanel.remove(downloadingItemFrame);
        this.revalidate();
        this.validate();
        this.repaint();
    }

}
