/**
 * This class is used to implement the panel of each file that is in queue.
 * this class extends class DownloadingItemFrame and adds a libel containing a string
 * which is "is waiting".
 *
 * @author Mohammad Jafari
 * @version 1.0
 * @since 5.9.2018
 */

import javax.swing.*;
import java.awt.*;
import java.util.Observer;

public class InQueueItem extends DownloadingItemFrame {

    public InQueueItem(DownloadInfo downloadInfo) {
        super(downloadInfo);

        observable = new StatusObservable();

        JLabel waitingLabel = new JLabel("...is waiting");
        JPanel southPanel = new JPanel(new BorderLayout(5,5));
        southPanel.add(waitingLabel,BorderLayout.EAST);
        southPanel.setBackground(Color.orange);
        this.add(southPanel,BorderLayout.SOUTH);
    }

    //overriding method of class CompletedItemFrame
    @Override
    public void addObserver(Observer observer) {
        super.addObserver(observer);
    }

}
